Git global setup  
git config --global user.name "username"  
git config --global user.email "your email id"  

Create a new repository  
git clone https://username@gitlab.com/bigopencloud/sb-demo.git  
cd sb-demo  
touch filename   
git add filename      
git commit -m "add filename"  
git push -u origin master  

Push an existing folder  
cd existing_folder  
git init  
git remote add origin https://username@gitlab.com/bigopencloud/sb-demo.git    
git add .  
git commit -m "Initial commit"  
git push -u origin master  

Push an existing Git repository  
cd existing_repo  
git remote rename origin old-origin  
git remote add origin https://username@gitlab.com/bigopencloud/sb-demo.git   
git push -u origin --all  
git push -u origin --tags  